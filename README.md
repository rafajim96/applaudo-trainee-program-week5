# Setup #

First you have to clone the project, all the .rb files for this project are on the develop branch.

To run this project you have to run the commands:

### rails db:create ###
### rails db:migrate ###
### rails db:seed ###
### rails server -p 3001 ###

*Make sure to change your postgres user if necessary on the database.yml

### Heroku Link ###
https://product-store-applaudo.herokuapp.com

Users:
Username: Eden - Password: root1234 (Admin)
Username: Neely - Password: root1234

### Endpoints ###

### Login ###
Sign in:
POST: http://localhost:3001/api/v1/users/sign-in
params: { username, password }

### Products ###
All products:
GET: http://localhost:3001/api/v1/products

Search by name:
GET: http://localhost:3001/api/v1/products
params: { name }

Pagination:
GET: http://localhost:3001/api/v1/products
params: { size, page }

Once the project is running you can access via the url's:

### Products table ###
http://localhost:3001/

### Login page ###
http://localhost:3001/users/sign-in
